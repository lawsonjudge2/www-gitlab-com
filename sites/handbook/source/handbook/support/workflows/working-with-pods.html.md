---
layout: handbook-page-toc
title: Working with Support Pods
category: Handling tickets
description: How to work with the Support Pod views on Zendesk
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is Support Pods?

Support Pods is an initiative to introduce subject-specific ticket views and ways that support engineers can work them in Zendesk. This was previously known as Areas of Focus. History and context behind this can be found in [this epic](https://gitlab.com/groups/gitlab-com/support/-/epics/145).  

There are many different uses of the word `pod` in the GitLab context. To remove ambiguity, support pods will always be referred to using the full term, never as `pods`. 

Note: we are [piloting this with a few support engineers](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3663), with the goal to roll this out globally.

## Why Support Pods?

Working on tickets with the Support Pods view has several positive impacts for both customers and us. Some of them are listed below, in no particular order:

1. Quicker ticket resolution and increased quality of support for customers.
1. Greater efficiency for support engineers through having a view of tickets with a similar theme.
1. Better ticket deflection and representation of customers' voice by improved closer relation with product teams.
1. More opportunities for intentional collaboration on tickets.
1. Focused learning opportunities and clearer sense of progression.

As a support engineer, you might sign up to a Support Pod for one of the following reasons:

- **Learning a topic** : Support Engineer is looking to up-skill in one or more topics covered by that support pod, and can do so by following along specific tickets, collaborating with experts, and deliberately owning tickets in that area to gain knowledge and confidence.
- **Strength on a topic** : Support Engineer is an expert in one or more topics covered by that support pod, and can help their peers and customers by engaging on tickets in that support pod.

## How can I integrate Support Pods in my day to day?

The [Key Principles](https://about.gitlab.com/handbook/support/workflows/working-on-tickets.html#key-principles) and [Priorities & Impact](https://about.gitlab.com/handbook/support/workflows/working-on-tickets.html#priorities-and-impact) guidelines are our guiding principles for working on tickets, keep these in mind as you work.

If you have signed up to a support pod, you can use the below prioritization guidelines.  A manager may ask you, or the team collectively, to focus on certain tickets or views from time to time, please help when you are able. 

1. Support Pod view 
    - This view is grouped by FRT & NRT. Refer to the guidlines for [meeting SLAs](https://about.gitlab.com/handbook/support/workflows/meeting-frt-sla.html#how-do-i-choose-between-needs-org--frt-and-handover-needed-view) when deciding whether to work from the FRT list or NRT list in this view.  (The same guideline applies to choosing which support pod view to work from if you have joined more than 1 support pod.)
    - The support pod views include assigned tickets from any preferred regions (including `All regions`) as well as unassigned tickets. This is different to the global ticket views of Needs-org/FRT and Handover Needed. See the [guidelines](#some-suggestions-for-working-in-support-pod-views) below for a suggested approach to interacting on tickets with an assignee.
1. Tickets you own Zendesk View: “My Assigned Tickets”
1. Everything else

## Can I sign up to more than 1 support pod?

Yes. The intent for signing up to a support pod can be different (topic of learning vs topic of strength), so you might have a need to sign up to more than 1 support pod at the same time.

It is recommended to sign up to a maximum of 2 support pods at a time, 1 for an area of your strength, and 1 for learning. This is recommended for the following reasons:

- It helps to ensure you do justice to both in terms of time.
- It minimizes the mental acrobatics required to decide which support pod view you'll take your next ticket from.

You can always sign out of a current support pod and sign up to a new support pod.

## How to sign up to a support pod

1. Prior to signing up, chat with your manager.
1. Create an [issue with Support Ops] to provide you access to the support pod view.

## Some suggestions for working in support pod views

Note that these are only guidelines, and you should feel free to make a judgement call based on the situation and your needs:

### Working in a support pod that is your topic of strength

1. Take assignment of New or unassigned tickets that you want to work on (unless they are High Priority and All Regions ([HPAR](https://about.gitlab.com/handbook/support/workflows/high-priority-all-regions-tickets-workflow.html#as-a-support-engineer-what-do-i-do-when-i-come-across-an-hpar-ticket))).
1. Add internal comments on tickets that are assigned and in progress with any assistance you can offer.
1. CC yourself on tickets to follow along and assist as needed.

### Working in a support pod that is your topic of learning

1. Review the tickets in the view.
    - Zendesk’s Play option is a good tool to use if you want to skim through the tickets.
    - Pairing with other support pod members is also a good way to learn.
1. Take assignment of New tickets and unassigned that you want to work on (unless they are High Priority and All Regions ([HPAR](https://about.gitlab.com/handbook/support/workflows/high-priority-all-regions-tickets-workflow.html#as-a-support-engineer-what-do-i-do-when-i-come-across-an-hpar-ticket))).
1. CC yourself on tickets that you want to follow along and learn from.

## Placeholder for current support pod groupings

## Placeholder for where to document support pod participants

## Placeholder for handovers

## Placeholder for handling Needs-org
